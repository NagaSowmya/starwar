import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import 'rxjs/Rx' 
import { Observable } from 'rxjs/Observable';
// import { HttpClientModule } from '@angular/common/http'; 
import { ApiProvider } from "../../providers/api/api";


/**
 * Generated class for the FilmsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-films',
  templateUrl: 'films.html',
})
export class FilmsPage {
  myUser : any;
  data1: any = [];
  // newUser : any;

  films: Observable<any[]>;
  httpClient: any;
datav : any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public apiProvider:ApiProvider ) {
   
    this.apiProvider.getFilms().subscribe((newUser)=>{
     
      this.myUser = newUser['results'];
      console.log(this.myUser);
     
        });

  //  this.films = this.apiProvider.getFilms();

}
openDetails(film) {
  this.navCtrl.push('FilmDetailsPage', {film: film});
}
// goToPlanets() {
//   this.navCtrl.parent.select(2);
// }

ionViewDidLoad() {
  console.log('ionViewDidLoad FilmsPage');
}

}

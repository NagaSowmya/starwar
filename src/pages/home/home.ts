import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
myVar:string ="Hey!!"
  constructor(public navCtrl: NavController) {

  }
  updateMyVal(){
    this.myVar="Hello StarWars !!";
  }

}
